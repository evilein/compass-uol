## 🪲**Sobre o Bug**

### »Bug ID:
_Informar qual o ID do Bug_

### »Data do Incidente:
_??/??/????_

### »Título do Bug:
_Informar tudo o que o tem no bug_

### »Quem está Reportando:
_Informar quem está reportando_

## 📝**Descrição do Bug**

### »URL:
_A URL em que ocorreu o bug_

### »Passos para Reprodução:
_Descrever passo a passo do bug, para reprodução_

### »Resultado Esperado:
_Qual é o resultado esperado?_

### »Resultado Obtido:
_Qual é o resultado realmente encontrado?_

### »Status:
_Informar o Status do Bug_

## ⚠️Severidade do Bug

### - [ ] S1 - Crítica
### - [ ] S2 - Grave
### - [ ] S3 - Moderada
### - [ ] S4 - Pequena

## 🚨Prioridade

### - [ ] P1 - Crítico
### - [ ] P2 - Alta
### - [ ] P3 - Média
### - [ ] P4 - Baixa

## 📸Screenshot:

### _Anexar uma imagem do Bug_
