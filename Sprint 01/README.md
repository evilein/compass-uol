# Sprint 01

<details>

<summary>Dia 01:: Ágil - 12/06/2023</summary>




## Tarefas:

- **Onboarding:** Explicação sobre a Compass UOL e o programa de bolsas;
- **Daily:** Conhecendo o Scrum Master e os colegas, e explicação do conteúdo;

## Conteúdo:
- **Git e Gitlab:** Aprendendo a usar.

  _Link do material complementar: [Comando Git](https://blog.geekhunter.com.br/comandos-git-mais-utilizados/)_

## Ajuda:

- Tive ajuda de **Kenny Keisuke** e **Filipe Alves** para solucionar um problema no git.


</details>

<details>

<summary>Dia 02:: Ágil - 13/06/2023</summary>




## Tarefas:

- **Daily:** Tiramos algumas dúvidas entre nós, participantes do programa de bolsas;
- Realização do curso **"Fundamentos do SCRUM Agile | Rápido e Prático"**;
-  Consumo dos materiais complementares, sobre o GitLab.

## Conteúdo:

- Curso "Fundamentos do SCRUM Agile | Rápido e Prático":

   -  Metodologia Tradicional e Ágil;

   -  Fluxo SCRUM;

   -  SCRUM na prática;

   -  Vantagens do SCRUM.

- _Link: [Epic, Feature and Story](https://odonodoproduto.com/epic-feature-and-story-epico-funcionalidade-e-historia/)_


- **Resumo:** [Dia 02](https://1drv.ms/w/s!AmleODTI8eCvgj-LHT2srVLF0Ma6?e=ljQiRR)

</details>

<details>

<summary>Dia 03:: MasterClass - 14/06/2023</summary>




## Tarefas:

-  Realização do curso **"Início Rápido em Teste e QA"**;

- **Daily:** Tiramos algumas dúvidas com o Scrum Master sobre a apresentação e o challenge da Sprint;

-  **Webinar | Caminhos de Sucesso pelo Programa de Bolsas:** Onde o convidado, Felipe da Silva, contou sobre sua experiência.

## Conteúdo:

- **Seção 01:**

    - Conhecendo sobre a carreira em Teste e QA;
    - Perfil do Profissional de Teste e QA:

        **1.** Soft Skills;

        **2.**	Hard Skills;

        **3.**	Débito Técnico.


- **Seção 02:**

    - Breve história do Teste;
    - Importância dos Testes e Danos dos Bugs;
    - Os 7 fundamentos do Teste (ISTQB):
    - Diferença entre Teste e QA;
    - Erro, Defeito e Falha.


- _**Resumo:** [Dia 03](https://1drv.ms/w/s!AmleODTI8eCvgkNm9ZSgOIe1sqfr?e=2CU0Pp)_

</details>

<details>

<summary>Dia 04:: MasterClass - 15/06/2023</summary>




## Tarefas:

- Continuação do curso **"Início Rápido em Teste e QA"**;
- **Daily:** Tirando algumas dúvidas sobre as atividades propostas.

## Conteúdo:

- **Finalização da Seção 02:**

    - Tipos de teste baseados na IEC/ISO 25010;
    - Testes manuais x Testes automatizados;
    - Testes ágeis x Teste tradicionais.

- **Seção 03:**

    - Pressão Organizacional;
    - Comprometido x Envolvido;
    - Autogerenciamento;
    - Comunicação verbal e não verbal;
    - Negociação (Ganha-Ganha);
    - Produtividade;
    - Fluxo contínuo;
    - Técnica pomodoro.


- _**Resumo:** [Dia 04](https://1drv.ms/w/s!AmleODTI8eCvgk1n9JuoxnOfURdu?e=EfQfRI)_

</details>

<details>

<summary>Dia 05:: MasterClass - 16/06/2023</summary>




## Tarefas:

- Finalização do curso **"Início Rápido em Teste e QA"**;
- **Daily:** Tiramos algumas dúvidas e o Scrum Master, por meio de sorteio, separou grupos para a realização de uma dinâmica.
- **Dinâmica:** Como gerar qualidade nos projetos?

    - _**Participantes do meu grupo:** Aline de Oliveira, Filipe Alves e Mateus Moreira._

## Conteúdo:

- **Curso "Início Rápido em Teste e QA":**

- **Seção 04:**
    - Planejamento de Testes.

- **Seção 05:**

    - O que é Análise de Teste;
    - O que é Modelagem;
    - O que é implementação;
    - Cenário: Ambiente;
    - Cenário: Situações;
    - Suítes de Teste;
    - Casos de Teste;
    - Introdução ao TDD, BDD E ATDD;
    - Testes unitários.

- _Links de materiais complementares da Dinâmica: [Proj4me](https://www.proj4.me/blog/gerenciamento-da-qualidade-em-projetos), [Qualidade em Projetos](https://blog.softexpert.com/qualidade-em-projetos/)._

- _Link da Dinâmica: [Como gerar Qualidade em um Projeto](https://compasso-my.sharepoint.com/:p:/g/personal/filipe_alves_pb_compasso_com_br/ETyEj47l66FAqKoObFCk3wgBCmdJPNyROZnAE5IcTCNc1w?e=pLaKqY)_

- _**Resumo:** [Dia 05](https://1drv.ms/w/s!AmleODTI8eCvglCZkWZdObADfmLN?e=cdUsSZ)_

</details>

<details>

<summary>Dia 06:: MasterClass - 19/06/2023</summary>




## Tarefas:

- Continuação dos conteúdos do dia 05;
- **Daily:** Tirando algumas dúvidas sobre o Challenge;
- Webinar Generative AI.

## Conteúdo:

- Fundamentos do Teste;
- Myers e Princípio de Pareto;
- Princípios do Teste de Software;
- _Link: [Princípios do Teste de Software](http://tmtestes.com.br/os-mais-importantes-principios-do-teste-de-software/)_

- Fundamentos do teste de software (backend);

- A pirâmide de teste.

- _Link: [A Pirâmide de Teste](https://medium.com/creditas-tech/a-pir%c3%a2mide-de-testes-a0faec465cc2)_ 

- _**Resumo:** [Dia 06](https://1drv.ms/w/s!AmleODTI8eCvglS5relQz0dNmjTJ?e=DSKzE4)_

## Ajuda:

- Tive ajuda de **Fábio Eloy** para me organizar melhor no GitLab.
</details>

<details>

<summary>Dia 07:: MasterClass - 20/06/2023</summary>




## Tarefas:

- Consumo do conteúdo **"Cloud Essentials Learning Plan"**;
- **Daily:** Tirando algumas dúvidas sobre o conteúdo de hoje e uma rápida explicação das próximas Sprints.

## Conteúdo:

- Job Roles in the Cloud;
- Getting Started with Cloud Acquisition;
- AWS Billing and Cost Management;
- Módulo 01 e 02 de "AWS Well-Architected".
- _**Resumo:** [Dia 07](https://1drv.ms/w/s!AmleODTI8eCvgnKaOI1-8h_jLbsW?e=C6rt0D)_

</details>

<details>

<summary>Dia 08:: MasterClass - 21/06/2023</summary>




## Tarefas:

- Continuação do conteúdo **"Cloud Essentials Learning Plan"**;
- **Daily:** Tiramos algumas dúvidas sobre o contéudo de hoje.

## Conteúdo:

- Finalização do "AWS Well-Architected";
- Fundamentos da AWS: Protegendo sua nuvem;
- AWS Foundations: Getting Started with the AWS Cloud Essentials;
- Módulo 01 até o 04 de "AWS Cloud Practitioner Essentials".
- _**Resumo:** [Dia 08](https://1drv.ms/w/s!AmleODTI8eCvhHZVfitgajlSB6A0?e=Jwsvhg)_

## Ajuda:
- Tive ajuda de **Fábio Eloy** para configurar o GitHub Desktop com o GitLab.

</details>

<details>

<summary>Dia 09:: MasterClass - 22/06/2023</summary>




## Tarefas:
- Finalização do conteúdo **"Cloud Essentials Learning Plan"**;
- **Daily:** Tirando algumas dúvidas sobre o conteúdo;
- Visualização do conteúdo sobre **"CyberSecurity"**.
## Conteúdo:
- Módulo 05 até o 11 de "AWS Cloud Practitioner Essentials";
- Webinar "Como ser hackeado e perder todas suas informações";
- Webinar "Novo OWASP TOP 10 - 2021".
- _*Resumo:* [Dia 09](https://1drv.ms/w/s!AmleODTI8eCvhSPHMqHwqm2KyUTN?e=JeQJDg)_

</details>

<details>

<summary>Dia 10:: Ágil - 23/06/2023</summary>




## Tarefas:
- Apresentação Challenge 01;
- Review Sprint.

</details>

